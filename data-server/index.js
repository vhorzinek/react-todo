const express = require("express");
const cors = require("cors");
const mysql = require("mysql");
const app = express();
app.use(cors());

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root123",
  database: "react-todo",
});

connection.connect((err) => {
  if (err) {
    return err;
  }
});

app.get("/tasks", (req, res) => {
  //Function to get all tasks from MySQL database
  const selectAll = "SELECT * FROM tasks";
  connection.query(selectAll, (err, results) => {
    if (err) {
      return res.send(err);
    } else {
      //return the results from db query in JSON format
      return res.json({
        data: results,
      });
    }
  });
});

app.get("/tasks/add", (req, res) => {
  //Deconstruct title object
  const { title } = req.query;
  //create query
  const insertTask = `INSERT INTO tasks (title) VALUES('${title}')`;
  //Commit query
  connection.query(insertTask, (err, results) => {
    if (err) {
      return res.send(err);
    } else {
      //Return confirmation on success
      return res.send("Task added");
    }
  });
});

app.get("/tasks/delete", (req, res) => {
  //Deconstruct title object
  const { id } = req.query;
  //create query
  const deleteTask = `DELETE FROM tasks WHERE id=${id}`;
  connection.query(deleteTask, (err, results) => {
    if (err) {
      return res.send(err);
    } else {
      //Return confirmation on success
      return res.send("Task removed");
    }
  });
});

app.listen(4000, () => {
  console.log("Server started on port 4000");
});
