import React, { useState, useEffect } from "react";
import "./App.css";
import firebase from "./firebaseConfig";
import AddTask from "./components/AddTask/AddTask";
import Task from "./components/Task/Task";

function App() {
  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  //Function to read data from firebase backend
  const getData = () => {
    var loadingTasks = [];
    firebase.db.collection("tasks").onSnapshot((snapshot) => {
      //gets called for every change in the database
      snapshot.docChanges().forEach((change) => {
        if (change.type === "added") {
          //gets called when data is added
          loadingTasks = [...loadingTasks];
          //push new data to the tasks array
          loadingTasks.push(change.doc.data());
          console.log(loadingTasks);
        }
        if (change.type === "removed") {
          //gets called when data is removed/deleted
          loadingTasks = loadingTasks.filter((task) => {
            //filter the deleted task out
            return task.id !== change.doc.data().id;
          });
        }
      });
      setTasks(loadingTasks);
    });
  };

  //Function to read data from the MySQL database
  const fetchData = () => {
    fetch("http://localhost:4000/tasks")
      .then((response) => response.json())
      .then(({ data }) => setTasks(data));
  };

  return (
    <div className="App">
      <main>
        <h1>React todo-List</h1>
        <AddTask fetchData={fetchData} />
        <div className="taskList">
          {tasks.length > 0 ? (
            tasks.map((task) => (
              <Task task={task} fetchData={fetchData} key={task.id} />
            ))
          ) : (
            <p>Keine Einträge</p>
          )}
        </div>
      </main>
    </div>
  );
}

export default App;
