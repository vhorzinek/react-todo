import * as firebase from "firebase/app";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyD1DyRXUiL0BGyT8w19fKQpKTiM57VvdY4",
  authDomain: "react-todo-883af.firebaseapp.com",
  databaseURL: "https://react-todo-883af.firebaseio.com",
  projectId: "react-todo-883af",
  storageBucket: "react-todo-883af.appspot.com",
  messagingSenderId: "572236990102",
  appId: "1:572236990102:web:034e54b8727530a45e64b7",
};

class Firebase {
  constructor() {
    firebase.initializeApp(firebaseConfig);
    this.db = firebase.firestore();
  }
}

export default new Firebase();
