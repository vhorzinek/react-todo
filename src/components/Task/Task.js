import React from "react";
import firebase from "../../firebaseConfig";
import { MdDelete } from "react-icons/md";
import "./Task.css";

const Task = (props) => {
  const deleteTask = () => {
    //Function to delete a task from the firebase database
    firebase.db.collection("tasks").doc(props.task.id).delete();

    /*Function to delete a task from the MySQL database 
    fetch(`http://localhost:4000/tasks/delete?id=${props.task.id}`)
      .then(props.fetchData)
      .catch((err) => console.error(err)); */
  };
  return (
    <div className="task">
      <div className="taskTitle">{props.task.title}</div>
      <button className="actionButton delete" onClick={deleteTask}>
        <MdDelete />
        Löschen
      </button>
    </div>
  );
};

export default Task;
