import React, { useState } from "react";
import firebase from "../../firebaseConfig";
import { MdAdd } from "react-icons/md";
import "./AddTask.css";

const AddTask = () => {
  const [task, setTask] = useState("");
  const submitTask = () => {
    if (task.trim() !== "") {
      //Function to add a task to the firebase database
      const dbRef = firebase.db.collection("tasks").doc();
      dbRef.set({
        title: task,
        created: new Date(),
        id: dbRef.id,
      });

      /*Function to add a task to the MySQL database

      fetch(`http://localhost:4000/tasks/add?title=${task}`)
        .then(props.fetchData)
        .catch((err) => console.error(err)); */
    }
  };
  return (
    <div className="addTask">
      <input
        onChange={(e) => setTask(e.target.value)}
        placeholder="Task eingeben..."
        type="text"
        value={task}
      />
      <button onClick={submitTask} className="addButton" type="submit">
        <MdAdd />
        Hinzufügen
      </button>
    </div>
  );
};

export default AddTask;
