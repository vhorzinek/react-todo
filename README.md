# React Todo

This is a React.js based Todo-list web app with a firebase backend.

A working demo can be found [here](https://react-todo-883af.firebaseapp.com/).

![Alt Text](./preview2.gif)

---

This project was the result of a React.js documentation I made for my school to guide students who might be interested in the process of creating and setting up a web app using React.js.
I chose Firebase and MySQL as database systems because I...
1.  ...wanted to show the differences between those two.
2.  And because I used firebase for my diploma thesis, and we use MySQL in school. 